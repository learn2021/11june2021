import json

#### json.loads() method can parse a json string and the result will be a Python dictionary ####
string = '{"id":1, "name": "Emily", "language": ["C++", "Python"]}'
read = json.loads(string)
pretty = json.dumps(read, indent=4)
print(pretty)


#### json.dumps() method can convert a Python object into a JSON string ####
dictionary = {"id": 2, "name": "Paul", "language": ["C++", "Python"]}
pretty1 = json.dumps(dictionary, indent=4)
print(pretty1)