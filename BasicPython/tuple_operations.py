###### Tuple cannot be modified, as it is immutable ######
my_tuple = (4, 6, 8, 11, 17, 25, 32)
print(my_tuple[0])

###### Unpacking #######
coordinates = (1, 2, 3)
x, y, z = coordinates
print(x)