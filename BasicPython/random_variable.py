import random

for i in range(3):
    print(random.randint(5, 25))

##########

members = ["Jyoti", "Saad", "Deepak", "Ankit", "Tapas"]
leader = random.choice(members)
print(leader)


###### Dice roll exercise ######
class Dice:
    def roll(self):
        first = random.randint(1, 6)
        second = random.randint(1, 6)
        return first, second


dice = Dice()
print(dice.roll())
