###### if Loop #####
is_hot = False
is_cold = False
if is_hot:
    print("It's a hot day, drink plenty of water")
elif is_cold:
    print("It's a cold day, wear warm clothes")
else:
    print("It's a lovely day")
print('Enjoy your day')

#### Exercise: Find the down payment for buying a house ####
price = 1000000
good_credit = True
if good_credit:
    down_payment = 0.1 * price
else:
    down_payment = 0.2 * price
    print('Down payment for buyer is: ' + str(down_payment))
print(f"Down payment for buyer is: ${down_payment}")

###### while Loop ######
i = 1
while i <= 5:
    print('*' * i)
    i += 1
print("Done")

###### Guessing Game ######
secret_num = 9
guess_count = 0
guess_limit = 3
while guess_count < guess_limit:
    guess = int(input('Guess: '))
    guess_count += 1
    if guess == secret_num:
        print("You Won!")
        break
else:
    print("Sorry, you failed!")


####### Car Game #######
command = ""
started = False
while command != "quit":
    command = input("> ").lower()
    if command == "start":
        if started:
            print("Car already started!")
        else:
            print("Car has started")
            started = True
    elif command == "stop":
        if not started:
            print("Car is already stopped!")
        else:
            print("Car is stopped")
            started = False
    elif command == "help":
        print("""
start - to start the car
stop - to stop the car
quit - to quit
        """)
    elif command == "quit":
        break
    else:
        print("No valid input")
