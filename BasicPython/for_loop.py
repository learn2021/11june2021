###### printing characters of a string ######
for item in 'Python':
    print(item)

###### printing strings in a list ######
for i in ['John', 'Ram', 'Shiva']:
    print(i)

###### printing objects in a range ######
for i in range(10):
    print(i)
###### printing numbers in a range ######
for i in range(5, 10):
    print(i)
###### printing numbers in range with a step of 2 ######
for i in range (1, 10, 2):
    print(i)

##### Exercise of calculating total cost of shopping cart #####
price = [10, 20, 30, 40]
total = 0
for i in price:
    total = total + i
print(f"Total: {total}")
