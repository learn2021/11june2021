###### printing coordinates ######
for x in range(4):
    for y in range(3):
        print(f"({x}, {y})")

##### Exercise of printing "F" shape using 'X' #####
# num = [5, 2, 5, 2, 2]
# for i in num:
#     for j in range(i-1):
#         print('X', end='')
#     print('X')
######### OR ##########
numbers = [5, 2, 5, 2, 2]
for x_count in numbers:
    output = ''
    for count in range(x_count):
        output += 'X'
    print(output)

##### Exercise of printing "L" shape using 'X' #####
l_num = [2, 2, 2, 8]
for k in l_num:
    result = ''
    for l in range(k):
        result += 'X'
    print(result)

###### Exercise of printing a triangle using '*' ######
for outer_count in range(8):
    tri_shape = ''
    for inner_count in range(outer_count):
        tri_shape += '*'
    print(tri_shape)

###### Exercise of printing a pyramid using '*' ######

