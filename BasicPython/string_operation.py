course = "Python's course for beginners"
print(course)

email = '''
Hi Jyoti,
This is a first email to you.
    Thank you,
    Course Team
'''
print(email)

###### Indiex of a string ######
my_course = 'Python for Beginners'
print(my_course[0])
print(my_course[7])
print(my_course[-1])
print(my_course[0:])
print(my_course[:5])
print(my_course[1:-1])

##### Formatted Strings ######
first = 'Jyoti'
last = 'Rout'
message = first + ' [' + last + '] is a tester'
print(message)
msg = f'{first} [{last}] is a tester'
print(msg)

###### String Methods #######
print(len(my_course))
print(my_course.upper())
print(my_course.lower())
print(my_course.title())
print(my_course.find('P'))
print(my_course.replace('Beginners', 'Absolute Beginners'))
print('Python' in my_course)
print('python' in my_course)