print(10 / 3)
print(10 // 3)
print(10 % 3)
print(10 ** 3)

x = 10
x = 10 + 3
print(x)
###### Augmented Assignment ######
x += 3
print(x)
x -= 3
print(x)

###### Operator Precedence ######
y = (10 + 4) * 3 ** 2
print(y)

##### Order of precedence #####
### 1. parenthesis
### 2. exponentiation 2**3
### 3. multiplication or division
### 4. addition or substraction

z = 2.9
n = -4.7
print(round(z))
print(abs(n))
import math
print(math.ceil(z))
print(math.floor(z))
