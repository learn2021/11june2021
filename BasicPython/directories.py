from pathlib import Path

#### Absolute Path (Complete path from the root directory)
# /usr/local/bin/

#### Relative Path (path from the current directory)

path = Path("packageexample")
print(path.exists())

#### search all the .py files in the directory ####
path1 = Path()
for file in path1.glob('*.py'):
    print(file)