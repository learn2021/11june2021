###### Dictionary is a collection of key(unique) value pair #######
customer = {
    "name": "Jyoti Rout",
    "age":  41,
    "is_verified": True
}
print(customer["name"])
print(customer.get("age"))

##### Updating value of a key in the dictionary #####
customer["name"] = "Soumya Rout"
print(customer["name"])

###### Adding key value pair to the dictionary ######
customer["birthday"] = "28 Jan 1982"
print(customer["birthday"])
print(customer)

###### Converting Numbers to Words ######
phone = input("Phone: ")
num2word = {
    "1" : "One",
    "2" : "Two",
    "3" : "Three",
    "4" : "Four"
}
output = ""
for digits in phone:
    output += num2word.get(digits, "!") + " "
print(output)

