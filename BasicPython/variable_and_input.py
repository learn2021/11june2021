full_name = input('What is your name? ')
age = input('What is your age? ')
print('Hi, I am ' + full_name + ' and I am ' + age + ' years old')

full_name = input('Enter person name ')
fav_color = input('Enter favorite color ')
print(full_name + ' likes ' + fav_color)

#### Finding age of person ####
birth_year = int(input('Year of birth: '))
print(type(birth_year))
age = 2022 - birth_year
print(type(age))
print(age)

#### Converting Pound to Kilogram ####
weight_lbs = input('Weight in pounds ? ')
weight_kg = int(weight_lbs) * 0.45
print(weight_kg)
print(type(weight_kg))
print('Weight in kilogram is: ' + str(weight_kg))
