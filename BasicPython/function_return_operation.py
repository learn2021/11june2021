##### Function and Arguments #####
def greet():
    print(f'Hi there!')
    print("Welcome")


print("Start")
greet()
print("Finish")


##### Function with one Parameter #####
def greet_user(name):
    print(f'Hi {name}!')
    print("Welcome")


print("Start")
greet_user("Jyoti")
greet_user("Tapas")
print("End")


####### Function with two Parameters #######
def greet_first_last(first_name, last_name):
    print(f'Hi {first_name} {last_name}!')
    print("Welcome")


print("Start")
greet_first_last("Jyoti", "Rout") ### Two positional arguments ###
print("Finish")


###### Keyowrd Argument ######
def greet_name(first_name, last_name):
    print(f'Hi {first_name} {last_name}!')
    print("Welcome")


print("Start")
greet_name("Jyoti", last_name="Rout") ### First positional argument ("Jyoti") and then keyword argument(last_name="Rout") not vice-versa ###
print("Finish")


##### Return Statement ######
def square(number):
    return number * number

print(square(4))


##### Reusable Function #####
def emoji_converter(message):
    words = message.split(" ")
    print(words)
    emojis = {
        ":)": "🙂",
        ":(": "😥"
    }
    output = ""
    for word in words:
        output += emojis.get(word, word) + " "
    return output


message = input(">")
print(emoji_converter(message))