### We create a separate module "converters.py" and import it here ###

import converters    ### you can import all the function inside the converters module
print(converters.kg_to_lbs(70))

#### OR ####

from converters import kg_to_lbs   ### you can import only specific function in the converters module
print(kg_to_lbs(62))

####### calling utils.py for max number #######
from utils import find_max
numbers = [2, 5, 10, 4, 25, 7]
maximum = find_max(numbers)
print(maximum)


##### calling the shipping module from the "packge_example" package #####
import packageexample.shipping
packageexample.shipping.calc_shipping()

##### OR #####

from packageexample.shipping import calc_shipping
calc_shipping()

###### OR #####
from packageexample import shipping
shipping.calc_shipping()




