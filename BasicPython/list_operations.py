###### Exercise to find the largest number in a list ######
my_list = [7, 5, 10, 1, 44, 23, 82, 44, 36]
max_num = my_list[0]
for item in my_list:
    if item > max_num:
        max_num = item
print(max_num)

###### Copy of an existing list ######
my_list2 = my_list.copy()
print(my_list2)

###### Removing duplicate items in a list ######
dup_num = [7, 5, 10, 1, 10, 44, 23, 82, 44, 36]
nodup_num = []
# 1 loop for original list and another for new list
for num in dup_num:
    if num not in nodup_num:
        nodup_num.append(num)
print(nodup_num)

###### Appending an item at the end of the list ######
my_list.append(99)
print(my_list)

###### Inserting an item into a certain position of the list ######
my_list.insert(0, 2)
print(my_list)

###### Removing an item from a certain position of the list ######
my_list.remove(5)
print(my_list)

##### Removing an item from the end of the list #####
my_list.pop()
print(my_list)
##### Finding the index of an item in the list #####
print(my_list.index(23))

###### Checking the existence of an item in the list ######
print(50 in my_list)

##### Counting the number of occurances of an item in the list #####
print((my_list).count(44))

###### Sorting of a list ######
my_list.sort()
print(my_list)

###### Reversing of a list ######
my_list.reverse()
print(my_list)

##### Removing all the items in a list #####
my_list.clear()
print(my_list)




