class Create:
    def move(self):
        print("move")
        
    def draw(self):
        print("draw")

create1 = Create()
create1.a = 10
create1.b = 20
print(create1.a)
create1.move()

##### Constructor #####
class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def move(self):
        print("move")

    def draw(self):
        print("draw")

point = Point(10, 20)
print(point.y)
point.y = 21
print(point.y)


#### Exercise ####
class Person:
    def __init__(self, name):
        self.name = name

    def talk(self):
        print(f'Hi, I am {self.name}')


person1 = Person("Jyoti Rout")
person1.talk()
person2 = Person("Harry Potter")
person2.talk()

