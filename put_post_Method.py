import requests
import json

def putmethod():
    URL = "https://jsonplaceholder.typicode.com/posts/1"
    paypay = {"id": 1, "title": "foo", "body": "bar", "userId": 1}
    payload = json.dumps(paypay)
    headers = {'Content-type': 'application/json'}
    response = requests.request("PUT", URL, data=payload, headers=headers)
    print(response.text)
    print(response.status_code)

def postmethod(url):

    #url = "https://jsonplaceholder.typicode.com/posts"
    paypay = {"id": 1, "title": "boo", "body": "car", "userId": 1}
    payload = json.dumps(paypay)
    headers = {'Content-type': 'application/json'}
    response = requests.request("POST", url, data=payload, headers=headers)
    print(response.text)
    print(response.status_code)


